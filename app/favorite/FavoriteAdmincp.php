<?php

/**
 * iCMS - i Content Management System
 * Copyright (c) 2007-2017 iCMSdev.com. All rights reserved.
 *
 * @author icmsdev <master@icmsdev.com>
 * @site https://www.icmsdev.com
 * @licence https://www.icmsdev.com/LICENSE.html
 */
class FavoriteAdmincp extends AdmincpBase
{
    public function __construct()
    {
        parent::__construct();
        $this->id    = (int) $_GET['id'];
    }
    public function do_add()
    {
        if ($this->id) {
            $rs = FavoriteModel::get($this->id);
        }
        include self::view("add");
    }
    public function ACTION_save()
    {
        $data = FavoriteModel::postData();
        $data['title']or self::alert('收藏夹标题不能为空');

        if (empty($data['id'])) {
            FavoriteModel::create($data, true);
            $msg = "收藏夹添加完成!";
        } else {
            FavoriteModel::update($data, $data['id']);
            $msg = "收藏夹编辑完成!";
        }
        // self::success('保存成功');
    }

    public function do_manage()
    {

        $keyword = Request::get('keyword');
        $keyword && $where['CONCAT(title,description)'] = array('REGEXP', $keyword);
        $userid = Request::get('userid');
        $userid && $where['uid'] = $userid;
        
        $orderby = self::setOrderBy();
        $result = FavoriteModel::where($where)
            ->orderBy($orderby)
            ->paging();

        include self::view("manage");
    }
    public function ACTION_delete($id = null)
    {
        $id === null && $id = $this->id;
        $id or self::alert('请选择要删除的收藏夹');
        FavoriteModel::delete($id);
        // $dialog && self::success('收藏夹已经删除');
    }
    public function ACTION_batch()
    {
        $stype = self::$BATCH['stype'];
        $actions = array(
            'dels' => function ($idArray, $ids, $batch) {
                foreach ($idArray as $id) {
                    $this->ACTION_delete($id, false);
                }
                // self::success('收藏夹全部删除完成');
            }
        );
        return self::batch($actions, "收藏夹");
    }

    public static function widget_count()
    {
        $total = FavoriteModel::count();
        $widget[] = array($total, '全部');
        return $widget;
    }
}
